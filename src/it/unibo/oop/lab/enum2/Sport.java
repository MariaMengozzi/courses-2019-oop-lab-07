/**
 * 
 */
package it.unibo.oop.lab.enum2;
import it.unibo.oop.lab.enum2.Place;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {
	BASKET(Place.INDOOR,5,"BASKET"),
	VOLLEY(Place.INDOOR,6,"VOLLEY"),
	TENNIS(Place.OUTDOOR,1,"TENNIS"), 
	BIKE(Place.OUTDOOR,1,"BIKE"),
	F1(Place.OUTDOOR,1,"F1"),
	MOTOGP(Place.OUTDOOR,1,"MOTOGP"),
	SOCCER(Place.INDOOR,11,"SOCCER");
	
	private final int noTeamMembers;
	private final String actualName;
	private final Place place;
    /*
     * TODO
     * 
     * [FIELDS]
     * 
     * Declare required fields
     */


     private Sport(final Place place, final int noTeamMembers, final String actualName) {
		this.noTeamMembers = noTeamMembers;
		this.actualName = actualName;
		this.place = place;
	}

     /**
      * 
      * @return true only if called on individual sports
      */
     public boolean isIndividual() {
    	 return this.noTeamMembers == 1;
     }
     /**
      * 
      * @return true in case the sport is practices indoor
      */
     public boolean isIndoorSport() {
    	 return this.place == Place.INDOOR;
     }
     
     /**
      * 
      * @return return the place where this sport is practiced
      */
     public Place getPlace() {
    	 return this.place;
     }
     
     /**
      * @return the string representation of a sport
      */
     public String toString() {
    	 return "[ " + this.actualName + " is played " + this.place + " the number of members of each team is" + this.noTeamMembers + " ]";
     }
}
