package it.unibo.oop.lab.nesting2;

import java.util.*;

public class OneListAcceptable<T> implements Acceptable<T> {
	
	private List<T> list = new ArrayList<>();

	public OneListAcceptable(final List<T> list){
		this.list = list;
	}
	
	public Acceptor<T> acceptor() {
		final Iterator<T> iterator = list.iterator();
		return new Acceptor<T>(){

			@Override
			public void accept(T newElement) throws Acceptor.ElementNotAcceptedException {
				try {
					if(newElement.equals(iterator.next())) {
						return;
					}
					
				}catch(Exception e) {
					System.out.println("no more elements to be evaluated");
				};
				throw new Acceptor.ElementNotAcceptedException(newElement);
			}

			@Override
			public void end() throws EndNotAcceptedException {
				try {
                    if (!iterator.hasNext()) {
                        return;
                    }
                } catch (Exception e) {
                    System.out.println("2" + e.getMessage());
                }
                throw new Acceptor.EndNotAcceptedException();
            }
		};
	}
}
